package steps;

import Base.BaseUtil;
import com.aventstack.extentreports.GherkinKeyword;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import pages.ProfilePage;

import java.util.concurrent.TimeUnit;

public class ProfileStep extends BaseUtil {
    private BaseUtil base;
    String winHandleBefore;
    ProfilePage page = new ProfilePage(base.Driver);

    public ProfileStep(BaseUtil base) {
        this.base = base;
    }

    @Given("I navigate to the profile page")
    public void i_navigate_to_the_profile_page() throws Throwable {
        base.scenarioDef.createNode(new GherkinKeyword("Given"), "I navigate to the profile page");
        System.out.println("Navigate to Profile Page");
        try {
            winHandleBefore = Driver.getWindowHandle();
            Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            page.ClickProfile();
            System.out.println("On pic upload");
            /** Switch to new window opened **/
            for (String winHandle : Driver.getWindowHandles()) {
                Driver.switchTo().window(winHandle);
            }
        }
        catch(Exception e){
            System.out.println("Exception occurred"+ e);
        }

    }

    @And("I am on linked photo item")
    public void i_am_on_linked_photo_item() throws Throwable{
        base.scenarioDef.createNode(new GherkinKeyword("And"), "I navigate to the picture upload page");
        System.out.println("Pointed the photo");
        page.ClickonLinkProfilePic();
        Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

    @And("I upload a profile picture")
    public void i_upload_a_profile_picture() throws Throwable{
        base.scenarioDef.createNode(new GherkinKeyword("And"), "I upload a profile picture");
        System.out.println("Apply the pic uploaded");
        Thread.sleep(3000);
        page.UploadPic();

    }

    @Then("I should see the profile picture")
    public void i_should_see_the_profile_picture() throws Throwable{
        base.scenarioDef.createNode(new GherkinKeyword("Then"), "I upload a profile picture");
        System.out.println("Picture uploaded");
        Thread.sleep(5000);
        page.savePic();
        System.out.println("Picture saved");
        Thread.sleep(15000);
        System.out.println("Closing the browser");
        Driver.close();

    }
}
