Feature: ProfileFeature
  This feature deals with the Profile functionality of the application

  Scenario: Navigate profile page of logged user and upload a profile picture
    Given I navigate to the profile page
    And I am on linked photo item
    And I upload a profile picture
    Then I should see the profile picture