package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.JavascriptExecutor;


import static Base.BaseUtil.Driver;

public class ProfilePage {

    public ProfilePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.XPATH, using = "//*[@href='/Ria18946025']")
    public WebElement gotoProfile;

    @FindBy(how = How.XPATH, using = "//*[text()='Edit profile']")
    public WebElement editProfile;

    @FindBy(how = How.XPATH, using = " //*[@data-testid='fileInput']")
    public WebElement profPic;

    @FindBy(how = How.XPATH, using = "//*[text()='Apply']")
    public WebElement applyPic;

    @FindBy(how = How.XPATH, using = "//*[text()='Save']")
    public WebElement savePic;



    public void ClickProfile()
    {
        gotoProfile.click();
        editProfile.click();
    }
    public void ClickonLinkProfilePic()
    {
        profPic.sendKeys("C:\\Sujay_Work\\seleniumcucumber\\pic\\cartoon.jpg");
    }
    public void UploadPic() {
        /*** Here using action class for a change ***/
        Actions actions = new Actions(Driver);
        actions.moveToElement(applyPic).click().perform();

    }

    public void savePic() {
        /*** Here using JavascriptExecutor for a change ***/
        JavascriptExecutor ex = (JavascriptExecutor)Driver;
        ex.executeScript("arguments[0].click();", savePic);

    }


}
